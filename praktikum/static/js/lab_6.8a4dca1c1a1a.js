var print = document.getElementById('print');
var erase = false;

var themes = [
        {"id":0,"text":"Red","bcgColor":"#F44336","fontColor":"#FAFAFA"},
        {"id":1,"text":"Pink","bcgColor":"#E91E63","fontColor":"#FAFAFA"},
        {"id":2,"text":"Purple","bcgColor":"#9C27B0","fontColor":"#FAFAFA"},
        {"id":3,"text":"Indigo","bcgColor":"#3F51B5","fontColor":"#FAFAFA"},
        {"id":4,"text":"Blue","bcgColor":"#2196F3","fontColor":"#212121"},
        {"id":5,"text":"Teal","bcgColor":"#009688","fontColor":"#212121"},
        {"id":6,"text":"Lime","bcgColor":"#CDDC39","fontColor":"#212121"},
        {"id":7,"text":"Yellow","bcgColor":"#FFEB3B","fontColor":"#212121"},
        {"id":8,"text":"Amber","bcgColor":"#FFC107","fontColor":"#212121"},
        {"id":9,"text":"Orange","bcgColor":"#FF5722","fontColor":"#212121"},
        {"id":10,"text":"Brown","bcgColor":"#795548","fontColor":"#FAFAFA"}
    ];

localStorage.setItem('themes', JSON.stringify(themes));
var retrievedObject = localStorage.getItem('themes');
var retrievedTheme = localStorage.getItem('newSelected');

// CALCULATOR
var go = function(x) {
  if (x === 'ac') {
    /* implemetnasi clear all */
    print.value = '';
  } else if (x === 'sin') {
    print.value = Math.sin(evil(print.value));
    erase = true;
  }
  else if (x === 'log') {
  		print.value = Math.log10(print.value);
  }
  else if (x === 'sqrt') {
    print.value = Math.sqrt(evil(print.value));
    erase = true;
  }
  else if (x === 'pow') {
    print.value = Math.pow(evil(print.value),2);
    erase = true;
  }
  else if (x === 'abs') {
    print.value = Math.abs(evil(print.value));
    erase = true;
  }
  else if (x === 'cos') {
    print.value = Math.cos(evil(print.value));
    erase = true;
  }
  else if (x === 'tan') {
    print.value = Math.tan(evil(print.value));
    erase = true;
  }
 
  else if (x === 'eval') {
      print.value = Math.round(evil(print.value) * 10000) / 10000;
      erase = true;
  } else {
    print.value += x;
  }
};

function evil(fn) {
  return new Function('return ' + fn)();
}
//END

// CHATBOX dan SELECT THEME
$(document).ready(function() {
  //Berbalas Chat
    var kelas = "msg-send";

    $('.chat-text textarea').keypress(function(e) {

      // 13 = key code nya ENTER
      if (e.keyCode == 13 && !e.shiftKey) {

        // Abis di ENTER gak ke NEWLINE
        e.preventDefault();

        var newMessage = $('textarea').val();
        var oldMessage = $('.msg-insert').html();

        $('.msg-insert').html(oldMessage + "<p class = '" + kelas + "'>" + newMessage + "</p>")

        if (kelas == "msg-send") {
          kelas = "msg-receive";
        } else {
          kelas = "msg-send";
        }

        // Abis submit, kosong lagi
        $('textarea').val("");
      }
    });
	//buka tutup
 $('img').bind("click", function () {
        $(".chat-body").toggle("slow");
        if(!hidden) {
            hidden = true;
            $("img").attr('src', up);
        } else {
          $("img").attr('src', down);
            hidden = false;
        }
    })

	//THEME
    $('#selector').select2({
        'data' : JSON.parse(retrievedObject)
    });
    var retrievedTheme = localStorage.getItem('newSelected');
    var appliedTheme = JSON.parse(retrievedTheme);
    $('body').css('background', appliedTheme.selected.bcgColor);
    $('body').css('color', appliedTheme.selected.fontColor);
});

// Change Theme
$('.apply-button').on('click', function(){  // sesuaikan class button
    // mengambil value dari elemen select .my-select
    var id = $(".my-select").select2("val");
    var data = $.parseJSON(retrievedObject);
    $.each(data, function(i, item) {
	    if(id == item.id){
	        var text = item.text;
	        var background = item.bcgColor;
	        var font = item.fontColor;

	        $('body').css("background-color",background);
	        $('body').css("color", font);

	        var pilih = {"selected":{"bcgColor":background,"fontColor":font, "text":text}};
	        
	        localStorage.setItem('newSelected', JSON.stringify(pilih));
	    }
    });
});

