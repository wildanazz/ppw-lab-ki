$(document).ready(function() {
    //Chat
      var chat = "msg-send";
  
      $('.chat-text textarea').keypress(function(e) {
  
        // 13 = The key code of ENTER
        if (e.keyCode == 13 && !e.shiftKey) {
  
          // after ENTER not go to new line
          e.preventDefault();
  
          var newMessage = $('textarea').val();
          var oldMessage = $('.msg-insert').html();
  
          $('.msg-insert').html(oldMessage + "<p class = '" + chat + "'>" + newMessage + "</p>")
  
          if (chat == "msg-send") {
            chat = "msg-receive";
          } else {
            chat = "msg-send";
          }
  
          // Empty after submit
          $('textarea').val("");
        }
      });
    });  