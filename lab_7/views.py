from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.views.decorators.csrf import csrf_exempt
from .models import Friend
from django.core import serializers
from .api_csui_helper.csui_helper import CSUIhelper
import os
import json


# Create your views here.
response = {}
csui_helper = CSUIhelper()

def index(request):
    mahasiswa_list = csui_helper.instance.get_mahasiswa_list()
    auth = csui_helper.instance.get_auth_param_dict() # import from csui_helper
    
    ############~Paginator~############
    page = request.GET.get('page', 1)
    paginate_data = paginate_page(page, mahasiswa_list)
    mahasiswa = paginate_data['data']
    page_range = paginate_data['page_range']
    ###################################
    
    friend_list = Friend.objects.all()
    response = {"mahasiswa_list": mahasiswa, "friend_list": friend_list, "author" : "Muhammad Wildan Aziz", "page_range": page_range, "auth": auth} # Create a response
    html = 'lab_7/lab_7.html'
    return render(request, html, response)

############~paginator~############
def paginate_page(page, data_list):
    paginator = Paginator(data_list, 10)

    try:
        data = paginator.page(page)
    except PageNotAnInteger:
        data = paginator.page(1)
    except EmptyPage:
        data = paginator.page(paginator.num_pages)

    # Get the index of the current page
    index = data.number - 1
    # This value is maximum index of your pages, so the last page - 1
    max_index = len(paginator.page_range)
    # You want a range of 10, so lets calculate where to slice the list
    start_index = index if index >= 10 else 0
    end_index = 10 if index < max_index - 10 else max_index
    # Get our new page range. In the latest versions of Django page_range returns 
    # an iterator. Thus pass it to list, to make our slice possible again.
    page_range = list(paginator.page_range)[start_index:end_index]
    paginate_data = {'data':data, 'page_range':page_range}
    return paginate_data
###################################

def friend_list(request):
    friend_list = Friend.objects.all()
    response['friend_list'] = friend_list
    response['author'] = "Muhammad Wildan Aziz"
    html = 'lab_7/daftar_teman.html'
    return render(request, html, response)

@csrf_exempt
def add_friend(request):
    if request.method == 'POST':
        name = request.POST['name']
        npm = request.POST['npm']
        
        ############~Adding friend code~############
        if not(Friend.objects.filter(npm=npm).exists()):
            if (npm != ''):
                friend = Friend(friend_name=name, npm=npm)
                friend.save()
                data = model_to_dict(friend)
                return HttpResponse(data)
        else:
            return HttpResponseBadRequest()
        ############################################

############~Model to dict~############     
def model_to_dict(obj):
    data = serializers.serialize('json', [obj])
    struct = json.loads(data)
    data = json.dumps(struct[0]["fields"])
    return data
#######################################

def delete_friend(request, friend_id):
    Friend.objects.filter(npm=friend_id).delete()
    return HttpResponseRedirect('/lab-7/get-friend-list/')

@csrf_exempt
def validate_npm(request):
    npm = request.POST.get('npm', None)
    data = {
        'is_taken': Friend.objects.filter(npm=npm).exists() # Check if friend with the npm is already available
    }
    return JsonResponse(data)

############~Get friend data~############
def get_friend_data(request):
    data = serializers.serialize('json', Friend.objects.all())
    struct = json.loads(data)

    data = []
    for i in range(len(struct)):
        data.append(eval(json.dumps(struct[i]["fields"])))
    return JsonResponse({"data":data})
#########################################

